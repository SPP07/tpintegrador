﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class lvlRestart : MonoBehaviour
{
    public Text txtGameOver;
    public Text txtRestart;
    void Update()
    {
        Restart();
        if (transform.position.y < -10f)
        {
            Restart();
        }
    }

    private void OnCollisionEnter(Collision collidingObject)
    {
        if (collidingObject.gameObject.layer == 8)
        {
            GameOver();
        }
    }

    public void Restart()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            Time.timeScale = 1;
        }
    }

    public void GameOver()
    {
        Time.timeScale = 0;
        txtGameOver.text = "GAME OVER";
        txtRestart.text = "Press 'R' to try again";
    }
}
