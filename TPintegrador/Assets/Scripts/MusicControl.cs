﻿using UnityEngine;

public class MusicControl : MonoBehaviour
{
    private void Start()
    {
        AudioManager.instancia.ReproducirSonido("TopGear");
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup") == true)
        {
            AudioManager.instancia.ReproducirSonido("RingSound");
        }
    }
}
