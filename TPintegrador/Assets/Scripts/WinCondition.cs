﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinCondition : MonoBehaviour
{
    public Text txtWin;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            txtWin.text = "LEVEL COMPLETE";
            Time.timeScale = 0;
        }
    }
}
