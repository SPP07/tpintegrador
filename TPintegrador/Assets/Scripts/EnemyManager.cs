﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyManager : MonoBehaviour
{
    public GameObject prefabEnemigo;

    void Start()
    {
        float corrimiento = 0.5f;

        for (int i = 0; i < 4; i++)
        {
            Instantiate(prefabEnemigo, new Vector3(2, corrimiento, 0), Quaternion.identity);
            corrimiento++;
        }

        DestruirATodosLosEnemigos();
    }

    void DestruirATodosLosEnemigos()
    {
        List<GameObject> listaEnemigos;
        listaEnemigos = GameObject.FindObjectsOfType<GameObject>().Where<GameObject>(ene => ene.name.Contains("Enemy")).ToList();

        foreach (GameObject ene in listaEnemigos)
        {
            Destroy(ene, 5);
        }
    }
}
