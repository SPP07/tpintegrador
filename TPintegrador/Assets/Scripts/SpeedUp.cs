﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedUp : MonoBehaviour
{
    public float multiplySpeed = 1.5f;
    public float duration = 3f;
    public Text txtSpeedUp;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(Pickup(other));
            txtSpeedUp.text = "SPEED UP!";
        }
    }

    IEnumerator Pickup(Collider Player)
    {
        PlayerMovement stat = Player.GetComponent<PlayerMovement>();
        stat.speed *= multiplySpeed;

        yield return new WaitForSeconds(duration);

        GetComponent<PlayerMovement>();
        stat.speed /= multiplySpeed;

        Destroy(gameObject);
        Destroy(txtSpeedUp);
    }
}
