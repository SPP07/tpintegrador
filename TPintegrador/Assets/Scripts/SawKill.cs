﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SawKill : MonoBehaviour
{
    private GameObject Player;
    public Text txtGameOver;
    public Text txtRestart;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player")
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        Time.timeScale = 0;
        txtGameOver.text = "GAME OVER";
        txtRestart.text = "Press 'R' to try again";
    }
}
