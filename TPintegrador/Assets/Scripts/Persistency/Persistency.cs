﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Persistency : MonoBehaviour
{
    public static Persistency instancia;
    public PersistencyData data;
    string archivoDatos = "save.data";
    void Awake()
    {
        if (instancia == null)
        {
            DontDestroyOnLoad(this.gameObject);
            instancia = this;
        }
        else if (instancia != this)
            Destroy(this.gameObject);
        LoadPersistencyData();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            Persistency.instancia.data.highscore = 5;
            SavePersistencyData();
        }
    }

    public void LoadPersistencyData()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();

        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            PersistencyData loaded = (PersistencyData)bf.Deserialize(file);
            data = loaded;
            file.Close();
            Debug.Log("Data loaded");
        }
    }

    public void SavePersistencyData()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Data saved");
    }
}
