﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public void Play()
    {
        SceneManager.LoadScene("TPintegrador");
    }

    public void Exit()
    {
        Debug.Log("You quit the game");
        Application.Quit();
    }
}
