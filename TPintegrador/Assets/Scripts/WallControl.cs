﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallControl : MonoBehaviour
{
    private int hp;
    void Start()
    {
        hp = 50;
    }

    private void takeDamage()
    {
        hp = hp - 5;

        if (hp <= 0)
        {
            this.Destroy();
        }
    }

    private void Destroy()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            takeDamage();
        }
    }
}
