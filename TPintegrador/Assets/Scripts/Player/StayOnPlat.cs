﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayOnPlat : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player")
        {
            collision.collider.transform.parent = transform;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        collision.collider.transform.parent = null;
    }
}
