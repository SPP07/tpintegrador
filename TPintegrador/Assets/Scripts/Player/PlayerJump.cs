﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    private Rigidbody rigidBody;
    public float JumpForce = 10;
    private const int maxJump = 1;
    private int jump = 0;
    private bool OnPlatform = true;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && (OnPlatform || maxJump > jump))
        {
            rigidBody.AddForce(Vector3.up * JumpForce, ForceMode.Impulse);
            OnPlatform = false;
            jump++;
        }
    }

    void OnCollisionEnter(Collision collidingObject)
    {
        if (collidingObject.gameObject.layer == 9)
        {
            OnPlatform = true;
            jump = 0;
        }
    }
}
