﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 10.0f;
    public int cont;
    public int highscore;
    public Text txtRing;
    public Text txtHighScore;
    public Camera Camera;
    public GameObject Proyectile;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        txtHighScore.text = "Highscore: " + highscore.ToString();
    }

    void Update()
    {
        float ForwardBack = Input.GetAxis("Vertical") * speed;
        float Side = Input.GetAxis("Horizontal") * speed;

        ForwardBack *= Time.deltaTime;
        Side *= Time.deltaTime;

        transform.Translate(Side, 0, ForwardBack);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject bul;
            bul = Instantiate(Proyectile, ray.origin, transform.rotation);

            Rigidbody rb = bul.GetComponent<Rigidbody>();
            rb.AddForce(Camera.transform.forward * 100, ForceMode.Impulse);

            Destroy(bul, 1);

        }

        if (cont > highscore)
        {
            highscore = highscore + 1;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup") == true)
        {
            cont = cont + 1;
            ShowText();
            other.gameObject.SetActive(false);
            AudioManager.instancia.ReproducirSonido("RingSound");
        }
    }

    private void ShowText()
    {
        txtRing.text = "Rings: " + cont.ToString();
        txtHighScore.text = "Highscore: " + cont.ToString();
    }
}
